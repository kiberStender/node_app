# What is this?

This repo repesents the pratical codes from my personal Blog [kiberBlog](https://kiberstender.github.io/). Ignore master branch for it will only have this instructional README file

# How to use it

To use it, you first clone it:
```sh
git clone https://bitbucket.org/kiberStender/node_app.git
cd app_node/
```


Then you have to checkout the branch representing the post you are reading. Usually in the beginning of the post I mention the branch that represents the last post you have to read before starting the one you are right now, so you know where to start from and in the end of the post I put the name of the branch representing that post you just read.

But if some reason you did not see it, the branches names are short versions of the post title, so if you are in the beginning of the post but wants to see the final code first([TL;DR;](https://www.dictionary.com/e/acronyms/tldr/)), check the post title and use the command:

```sh
git branch -a
```

This will list all remote branches to you, then look for the one whose name is the closest to the post you are about to read right now and then:

```sh
git checkout --track origin/<post_title>

```

All branches will have only stuff related to the post it represents, so if for some reason I improve the code, it will be reflected in the post, so you will not find a different code here than the one you can produce reading the blog. This branches are for question solving in case I explained something in a way you did not understand and looking at the code can help you understand why or how I did it.

# The code

This code is completely free of charge and to be used anywhere, you don't even have to give me any credits, just be happy and use it as you are pleased.

# Suggestions e comments

For now I do not have comment section so if you have any suggestion or fix to any mistake I commited, go to the file you found the issue and create a Pull request explaning me what I can do better, how and why. As the purpose of this blog is not only tell you what to do, but how and why you should do this way to incentivate rational thinking and not copy and paste only
